<?php

namespace Drupal\instagram_graphql\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'Instagram GraphQL' formatter.
 *
 * @FieldFormatter(
 *   id = "instagram_graphql_formatter",
 *   label = @Translation("Instagram GraphQL"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class InstagramGraphQLFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#theme' => 'instagram_graphql',
        '#graphql_arguments' => [
          'url' => "https://www.instagram.com/{$item->value}/?__a=1",
          'sharedData' => $this->getSharedData($item->value),
        ],
      ];

      // Plan C: By Javascript...
      $element[$delta]['#attached']['drupalSettings']['instagram']['account'] = $item->value;
      $element[$delta]['#attached']['library'][] = 'instagram_graphql/fetcher';
    }

    return $element;
  }

  /**
   * Another fancy way to extract the data from Instagram.
   *
   * @param string $username
   *
   * @return mixed
   */
  protected function getSharedData($username) {
    $url     = sprintf("https://www.instagram.com/$username");
    $content = file_get_contents($url);
    $content = explode("window._sharedData = ", $content)[1];
    $content = explode(";</script>", $content)[0];
    $data    = json_decode($content, true);
    $rawData = NestedArray::getValue($data, [
      'entry_data',
      'ProfilePage',
      0,
      'graphql',
      'user',
      'edge_owner_to_timeline_media',
      'edges',
    ]);
    return array_map(function($item) {
      return [
        'owner' => NestedArray::getValue($item, [
          'node',
          'owner',
          'username',
          ]),
        'url' => NestedArray::getValue($item, [
          'node',
          'display_url',
        ]),
        'caption' => NestedArray::getValue($item, [
          'node',
          'accessibility_caption',
        ]),
        'description' => NestedArray::getValue($item, [
          'node',
          'edge_media_to_caption',
          'edges',
          0,
          'node',
          'text',
        ]),
      ];
    }, $rawData ?? []);
  }

}
