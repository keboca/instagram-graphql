(function ($) {
  Drupal.behaviors.instagramFetcher = {
    attach: function (context, settings) {
      // Apply jQuery.once behavior.
      $('.field--name-field-instagram-account', context)
        .once('instagram-fetcher')
        .each(function () {
          // Verify instagram settings are available.
          if (typeof drupalSettings.instagram === "object") {
            // Consume Instagram account homepage by HTTP GET method.
            var url = 'https://www.instagram.com/' + settings.instagram.account;
            $.ajax({
              type: 'GET',
              url: url,
              error: function (e) {
                console.error(e);
              },
              success: function (response) {
                var data = Drupal.behaviors.instagramFetcher.prepareResponse(response);
                if (data !== null) {
                  var cards = Drupal.behaviors.instagramFetcher.buildCards(data);
                  console.log(cards);
                }
              }
            });
          }
        });
    },
    prepareResponse: function (response) {
      var data = null;
      try {
        var entry = JSON.parse(
          response.split("window._sharedData = ")[1]
            .split(";</script>")[0]
        ).entry_data;

        if(entry.ProfilePage) {
          data = entry.ProfilePage[0].graphql;
        }
      } catch (e) {
        console.error(e);
      }

      return data;
    },
    buildCards: function (data)  {
      if (typeof data.user === 'object' &&
        typeof data.user.edge_owner_to_timeline_media === 'object' &&
        typeof data.user.edge_owner_to_timeline_media.edges === 'object') {

        var cards = [];
        var edges = data.user.edge_owner_to_timeline_media.edges;
        for (var idx in edges) {
          var edge = edges[idx].node;
          cards.push({
            'username': edge.owner.username,
            'image_url': edge.display_url,
            'image_caption': edge.accessibility_caption,
            'description': edge.edge_media_to_caption.edges[0].node.text
          });
          if (cards.length >= 10) {
            break;
          }
        }
        return cards;
      }

    }
  };
})(jQuery);
